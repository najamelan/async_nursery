# async_nursery - CHANGELOG

## 0.3.0-beta.1

  - BREAKING CHANGE: Update _async_executors_ to 0.4.0-beta.1. Will drop beta when tokio releases 1.0
  - add proper support for tracing `Instrument` and `WithDispatch`.
  - remove thiserror dependency.

## 0.2.0 - 2020-06-11

  - Update _async_executors_ to 0.3.

## 0.1.0 - 2020-04-30

  - Initial release.




